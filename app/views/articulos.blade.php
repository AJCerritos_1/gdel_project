@extends('layouts.master')

@section('content')

		<div class="jumbotron" id="jumAsesoria">
			<div class="row">
				<div class="col-lg-6">
					<H3>LIDERAZGO/TRASCENDENCIA</H3>
				</div>
				<div class="col-lg-6">
					<H3>OPINION/ACTUALIDAD</H3>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-lg-12">
					<p class="navbar-text">¡Raros!:</p>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<p class="navbar-text">
					<small>Un poco de aprendizaje social... </small><span class="label label-default" id="trigger"><a style="color:white;" class="external" href="" data-toggle="modal">ver más</a></span>
					</p>
				</div>
			</div>
      </div>
	<hr>

	<div id="dialog" title="¡Raros!" style="display:none;width:65%;">
		<iframe src="./resources/Raros.pdf"></iframe>
	</div>

	<!--Modals-->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>
		
@stop