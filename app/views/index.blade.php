@extends('layouts.master')
 
@section('content')
	<!-- Full Width Image Header -->

    <!-- Page Content -->
    <div class="container">
		<div class="row">
			<div class="col-sm-10 col-md-10 col-sm-offset-1 col-md-offset-1">
				<div id="carousel_index" class="carousel slide">
				  <!-- Wrapper for slides -->
				  <div class="carousel-inner">
					<div class="item active">
					  {{ HTML::image('images/logo_index.png', 'banner1', array('class' => 'img-responsive')) }}
					</div>
					<div class="item">
					  {{ HTML::image('images/liderazgo_index.png', 'banner1', array('class' => 'img-responsive')) }}
					</div>
				  </div>

				  <!-- Controls -->
				  <a class="left carousel-control" href="#carousel_index" data-slide="prev" style="height: 70%;background: transparent;">
					<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				  </a>
				  <a class="right carousel-control" href="#carousel_index" role="button" data-slide="next" style="height: 70%;background: transparent;">
					<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				  </a>
				</div>
			</div>
		</div>
        <hr>
		<br />
        <div class="row" id="renglon_index">
            <div class="col-sm-4">
				{{ HTML::image('images/liderazgo.jpg', 'Responsive image', array('class' => 'img-circle img-responsive img-center grayscale')) }}
				<blockquote>
					<h2 class="text-center"><span>PASIÓN</span></h2>
					<h3 class="text-center"><span>LIDERAZGO</span></h3>
				</blockquote>
            </div>
            <div class="col-sm-4">
                {{ HTML::image('images/profesionalismo.jpg', 'Responsive image', array('class' => 'img-circle img-responsive img-center grayscale')) }}
				<blockquote>
					<h2 class="text-center"><span>ELEGANCIA</span></h2>
					<h3 class="text-center"><span>PROFESIONALISMO</span></h3>
				</blockquote>
            </div>
            <div class="col-sm-4">
                {{ HTML::image('images/competitividad.jpg', 'Responsive image', array('class' => 'img-circle img-responsive img-center grayscale')) }}
				<blockquote>
					<h2 class="text-center"><span>MOTIVACIÓN</span></h2>
					<h3 class="text-center"><span>COMPETITIVIDAD</span></h3>
				</blockquote>
            </div>
        </div>
		<br class="hidden-xs" />
		<br class="hidden-xs" />
		<br class="hidden-xs" />
        <div class="row" id="renglon_index">
            <div class="col-sm-offset-2 col-sm-4">
				{{ HTML::image('images/exito.jpg', 'Responsive image', array('class' => 'img-circle img-responsive img-center grayscale')) }}
                <blockquote>
					<h2 class="text-center"><span>VOLUNTAD</span></h2>
					<h3 class="text-center"><span>ÉXITO</span></h3>
				</blockquote>
            </div>
            <div class="col-sm-4">
                {{ HTML::image('images/desicion.jpg', 'Responsive image', array('class' => 'img-circle img-responsive img-center grayscale')) }}
				<blockquote>
					<h2 class="text-center"><span>DESICIÓN</span></h2>
					<h3 class="text-center"><span>FUERZA</span></h3>
				</blockquote>
            </div>
        </div>
        <hr>		
    </div>
    <!-- /.container -->
@stop