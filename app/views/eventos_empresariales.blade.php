@extends('layouts.master')

@section('content')
	<div class="blog-header">
		<div class="col-md-8 col-lg-8">
			 <div class="jumbotron" id="jumAsesoria" style="margin-bottom: 1em;padding: 1em 2em 2em;padding: 0em|;">
				<h3 class="text-danger" style="color:white;">Líder en organización de eventos empresariales y corporativos</h3>
			  </div>
		</div>
    </div>

      <div class="row">

        <div class="col-sm-8 blog-main">
			
          <div class="blog-post">
			<br />
				<blockquote>
				  <p>Nuestra principal fortaleza es el estudio profesional y actualizado de los mejores y más competitivos sistemas de <strong>LOGÍSTICA ORGANIZACIONAL Y CORPORATIVA</strong>, apoyados por la <strong>ELEGANCIA Y ALTURA PROFESIONAL</strong></p>
				</blockquote>
			<hr>
				<pre><code>Nuestra experiencia dentro del ramo de Organización de Eventos, nos permite realizar lo siguiente:</code></pre>
			<br />
				<dl>
				  <dt>Imagen del evento:</dt>
				  <dd>Slogan, Promoción y Publicidad del evento.</dd>
				  <br />
				  <dt>Logística general:</dt>
				  <dd>Ponentes, maestro de ceremonias, expositores, avales.</dd>
				  <br />
				  <dt>Mesas de Registro:</dt>
				  <dd>con software de captura, y entrega de gafetes.</dd>
				  <br />
				  <dt>Salón Plenario:</dt>
				  <dd>Audio, Video, Circuito cerrado de proyección, Pantallas de acuerdo al número de asistentes, iluminación, monitores para el expositor y control de su ponencia y si es necesario TRADUCTOR SIMULTANEO.</dd>
				  <br />
				  <dt>Mesas de refrigerio y Snack.</dt>
				  <br />
				  <dt>Material de Trabajo:</dt>
				  <dd>Carpetas, gafetes, reconocimientos, base de datos de asistentes, sesión de preguntas y respuestas.</dd>
				  <br />
				  <dt>Contamos con transporte privado para el traslado de los expositores y conferencistas.</dt>
				  <br />
				  <dt>Video Resumen del Evento:</dt>
				  <dd>Se filma durante todo el evento y se entrega video editado al finalizar el evento.</dd>
				  <br />
				  <dt>Finalmente otorgamos el reconocimiento al expositor e invitados que debidamente sea indicado por la Empresa anfitriona.</dt>
				</dl>
            <hr>
          </div><!-- /.blog-post -->

        </div><!-- /.blog-main -->

        <div class="col-sm-3 col-sm-offset-1 blog-sidebar">
			<div class="col-xs-6 col-sm-12 col-md-12 col-lg-12">
			{{ HTML::image('images/Eventos_Empresariales_1.jpg', 'Responsive image',array('class' => 'img-thumbnail img-responsive grayscale')) }}
			</div>
			<div class="col-xs-6 col-sm-12 col-md-12 col-lg-12">
			{{ HTML::image('images/Eventos_Empresariales_2.jpg', 'Responsive image',array('class' => 'img-thumbnail img-responsive grayscale')) }}
			</div>
			<div class="col-xs-6 col-sm-12 col-md-12 col-lg-12">
			{{ HTML::image('images/Eventos_Empresariales_3.jpg', 'Responsive image',array('class' => 'img-thumbnail img-responsive grayscale')) }}
			</div>
			<div class="col-xs-6 col-sm-12 col-md-12 col-lg-12">
			{{ HTML::image('images/Eventos_Empresariales_4.jpg', 'Responsive image',array('class' => 'img-thumbnail img-responsive grayscale')) }}
			</div>
        </div><!-- /.blog-sidebar -->

      </div><!-- /.row -->
@stop