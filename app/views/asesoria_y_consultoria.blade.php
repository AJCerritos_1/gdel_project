@extends('layouts.master')

@section('content')
	  <!-- Jumbotron -->
      <div class="jumbotron" id="jumAsesoria">
        <h1 class="text-danger">¡Asesoría y consultoría empresarial!</h1>
        <p><a class="btn btn-danger btn-sm" href="#" role="button">Tu guía hacia el éxito empresarial</a></p>
      </div>

      <!-- Example row of columns -->
      <div class="row">
        <div class="col-lg-6">
          <p class="text-justify">
		  Dirigida a instituciones públicas y privadas interesadas en optimizar sus recursos
		  humanos, ...
		  </p>
        </div>
        <div class="col-lg-6">
			<center>
				<strong>
					GDEL, S.A. de C.V. ofrece una amplia gama <br />
					de recursos, conferencias y congresos con...
				</strong>
			</center>
			<br />
          <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
       </div>
      </div>
	
@stop