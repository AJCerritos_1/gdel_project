<?php 
Route::get('/', function()
{
    return View::make('index');
});

Route::get('nosotros', function()
{
    return View::make('nosotros');
});

Route::get('articulos', function()
{
    return View::make('articulos');
});

Route::get('contacto', function()
{
    return View::make('contacto');
});

Route::get('asesoria_consultoria', function()
{
    return View::make('asesoria_y_consultoria');
});

Route::get('eventos_empresariales', function()
{
    return View::make('eventos_empresariales');
});

Route::get('programas_liderazgo', function()
{
    return View::make('programas_de_liderazgo');
});

Route::get('coordinacion_campanias', function()
{
    return View::make('coordinacion_de_campanias');
});

Route::get('executive_bussines', function()
{
    return View::make('executive_bussines');
});

Route::get('noticias', function()
{
    return View::make('noticias');
});

 
Route::get('usuarios', array('uses' => 'UsuariosController@mostrarUsuarios'));
 
Route::get('usuarios/nuevo', array('uses' => 'UsuariosController@nuevoUsuario'));
 
Route::post('usuarios/crear', array('uses' => 'UsuariosController@crearUsuario'));
// esta ruta es a la cual apunta el formulario donde se introduce la información del usuario 
// como podemos observar es para recibir peticiones POST 
 
Route::get('usuarios/{id}', array('uses'=>'UsuariosController@verUsuario'));
// esta ruta contiene un parámetro llamado {id}, que sirve para indicar el id del usuario que deseamos buscar 
// este parámetro es pasado al controlador, podemos colocar todos los parámetros que necesitemos 
// solo hay que tomar en cuenta que los parámetros van entre llaves {}
// si el parámetro es opcional se colocar un signo de interrogación {parámetro?}
 
 
?>